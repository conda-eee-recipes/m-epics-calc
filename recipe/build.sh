#!/bin/bash

# Build the EPICS module
# Override PROJECT and LIBVERSION
# - PROJECT can't be guessed from the working directory
# - If we apply patches, the version will be set to the username
make PROJECT=calc LIBVERSION=3.6.1
make PROJECT=calc LIBVERSION=3.6.1 install

# Clean builddir between variants builds
rm -rf builddir
